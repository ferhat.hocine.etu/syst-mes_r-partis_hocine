package systemes.repartie.tree.ftp.FtpConnection;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import systemes.repartie.tree.ftp.ParametreConnection.ConnectionParametre;

/**
 * Connexion class with ftp server
 * @author hocine
 *
 */
public class Connection {
	// variable declaration
	private ConnectionParametre connect;
	private Socket socket;
	private BufferedReader bfR;
	private OutputStream outPut;
	private PrintWriter printer;
	//Constructor
	public Connection(ConnectionParametre connect) {
		this.connect=connect;
		this.socket=null;
		this.bfR=null;

	}
	
	/**
	 * return the instance of ConnectionParametre used by our class
	 */
	public ConnectionParametre getConnectionParam() {
		return this.connect;
	}
	
	/**
	 * Connexion to ftp server methode
	 * this methode uses our instance of ConnectionParametre to connect to ftp server
	 * @throws IOException
	 */
	public void OperationConnection() throws IOException{
		//creating a new instance of socket using ConnectionParametre adress and port
		this.socket= new Socket(this.connect.getAdresse(),this.connect.getPort());
		//creating a new instance of Buffered Reader
		this.bfR = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		//creating a new instance of printer of Buffered writer
		printer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream())));
		String Errors="";
		//Connecting
		String r = this.bfR.readLine();
		//checking errors
		if (!r.startsWith("220")) {	Errors=Errors+" FTP Error --> "+r;}	
		printer.write("AUTH TLS\r\n");
		printer.flush();
		r = this.bfR.readLine();
		//checking errors
		if (!r.startsWith("530")&& Errors=="") {	Errors=Errors+" AUTH TLS Error --> "+r;}	
		
		printer.write("AUTH SSL\r\n");
		printer.flush();
		r = this.bfR.readLine();
		//checking errors
		if (!r.startsWith("530")&& Errors=="") {	Errors=Errors+" AUTH SSL Error --> "+r;}				
		//identification using ConnectionParametre user and passWord
		printer.write("USER "+this.connect.getIdentifiant()+"\r\n");
		printer.flush();
		r = this.bfR.readLine();
		//checking errors
		if (!r.startsWith("331")&& Errors=="") {	Errors=Errors+" USER Error --> "+r;}				
				
		printer.write("PASS "+this.connect.getMotdepasse()+"\r\n");
		printer.flush();
		r = this.bfR.readLine();
		//checking errors
		if (!r.startsWith("230")&& Errors=="") {	Errors=Errors+" PASS Error --> "+r;}				
		
		//Print result if succeed or throwing exception if failed  
		if(Errors=="") {
			System.out.println("Login successful.");
		}
		else {
			throw new IOException("Errors: "+Errors);
		}
		}
	
	/**
	 * executing PWD commande to get repertory root 
	 * @throws IOException
	 */
	public void envoyerPWD()  throws IOException {
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
		}
		else {
		try {
			printer.write("PWD\r\n");
			printer.flush();
			String r = this.bfR.readLine();
		}
		catch(IOException e) {
			throw new IOException("Buffer reader error");
		}
		}
	}
	
	/**
	 * executing CWD command using the directory given in param to access to it
	 * @param dossier 
	 * @throws IOException
	 * @return boolean : True if it succeed to access to the directory or False if it failed
	 */
	public boolean envoyerCWD(String dossier) throws IOException  {
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
		}
		else {
			try {
				printer.write("CWD "+dossier+"\r\n");
				printer.flush();
				String r = this.bfR.readLine();
				return r.startsWith("250");
				
				}
				catch(IOException e) {
						throw new IOException("Buffer reader error");
					}
				catch (NullPointerException e){
					return false;
			}
		}
		return false;
	}
	
	/**
	 * executing PASV command to enter to passive mode
	 * @throws IOException
	 * @return String : the information received in passive mode
	 */
	public String envoyerPASV() throws IOException  {
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
			return "";
		}
		else {
		try {
			printer.write("PASV\r\n");
			printer.flush();
			String r = this.bfR.readLine();
			return r;
		}
		catch(IOException e) {
			throw new IOException("Buffer reader error");
		}
		}
	}
	
	/*
	 * getting Ip address from passive mode information
	 * String String : PASV 
	 * @return String : the Ip address extracted from passive mode information
	 */
	public String getAdressePasv(String PASV) {
		int i = PASV.indexOf("(");
		int j = PASV.indexOf(")");
		PASV= PASV.substring(i+1,j);
		String [] str=PASV.split(",");
		String res="";
		for (i=0; i<4; i++) {
			res+=str[i]+".";
		}
		res=res.substring(0,res.length()-1);
		return res;
		}
	
	/*
	 * calculation of the Port from passive mode information
	 * String String : PASV 
	 * @return int : Port extracted then calculated from passive mode information
	 */
	public int getPORTPasv(String PASV) {
		String [] str=PASV.split(",");
		return (Integer.parseInt(str[str.length-2])*256 )+ Integer.parseInt(str[str.length-1].substring(0,str[str.length-1].length()-2));
	}
	
	
	/*
	 * executing  TYPE I command to switch to the binary mode
	 * @throws IOException
	 */
	public void envoyerType() throws IOException  {
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
		}
		else {
		try {
			printer.write("TYPE I\r\n");
			printer.flush();
			String r = this.bfR.readLine();
		}
		catch(IOException e) {
			throw new IOException("Buffer reader error");
		}
		}
	}
	
	/*
	 * executing LIST command to get the List of file,directory and link in our repertory
	 * @throws IOException
	 * @return ArrayList<String> : the List  of file,directory and link in our repertory
	 */
	public ArrayList<String> envoyerList(String adrs, int port) throws IOException  {
		ArrayList<String> Readl=new ArrayList<String>();
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
			return Readl;
		}
		else {
			try {
				printer.write("LIST\r\n");
				printer.flush();
				Readl=this.newConnection(adrs, port);
				String r = this.bfR.readLine();
				r = this.bfR.readLine();
				
				return Readl;
		}
		
		catch(IOException e) {
			throw new IOException("Buffer reader error");
		}
	}
	}
	
	
	/*
	 * executing  Cdup To change directory
	 * @throws IOException
	 */
	public void envoyerCdup() throws IOException  {
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
		}
		else {
		try {
			printer.write("CDUP\r\n");
			printer.flush();
			String r = this.bfR.readLine();
		}
		catch(IOException e) {
			throw new IOException("Buffer reader error");
		}
		}
	}
	
	/*
	 * executing  Quit To QUIT socket
	 * @throws IOException
	 */
	public void envoyerQuit() throws IOException  {
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
		}
		else {
		try {
			printer.write("QUIT\r\n");
			printer.flush();
			String r = this.bfR.readLine();
		}
		catch(IOException e) {
			throw new IOException("Buffer reader error");
		}
		}
	}
	
	/*
	 * Staring new socket the to get the List of file,directory and link in our repertory from IP Address and Port
	 * @param  Adresse : IP address
	 * @param  Port : port
	 * @throws IOException
	 * @return ArrayList<String> : The List of file,directory and link in our repertory from IP Address and Port
	 */
	public ArrayList<String> newConnection(String Adresse,int Port) throws UnknownHostException, IOException{
		ArrayList<String> liste=new ArrayList<String>();
		Socket socket1= new Socket(Adresse,Port);
		BufferedReader bfR1 = new BufferedReader(new InputStreamReader(socket1.getInputStream()));
		OutputStream outPut1 = socket1.getOutputStream();
		PrintWriter printer1 = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket1.getOutputStream())));
		try {
		String r = bfR1.readLine();
		while (r!=null) {
			liste.add(r);
			r = bfR1.readLine();
		}
		printer1.write("QUIT\r\n");
		printer1.flush();
		return liste;	
		}
		catch(IOException e) {
			throw new IOException("Buffer reader error");
		}
	}
	
	/*
	 * Close Socket
	 */
	public void closeSocket() throws IOException {
		this.socket.close();
	}

}
