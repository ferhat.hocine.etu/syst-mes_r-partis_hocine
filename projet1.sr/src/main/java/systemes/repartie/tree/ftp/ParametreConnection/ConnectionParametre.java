package systemes.repartie.tree.ftp.ParametreConnection;

/**
 * Params class to identify user
 * @author hocine
 *
 */
public class ConnectionParametre {
	// variable declaration
	private String identifiant;
	private String motdepasse;
	private String adresse;
	private int port;
	//Constructor
	public ConnectionParametre(String identifiant,String motdepasse,int port,String adresse) {
		this.identifiant=identifiant;
		this.motdepasse=motdepasse;
		this.port=port;
		this.adresse=adresse;
	}
	
	//getters
	
	public String getIdentifiant() {
		return this.identifiant;
	}
	public String getMotdepasse() {
		return this.motdepasse;
	}
	public String getAdresse() {
		return this.adresse;
	}
	public int getPort() {
		return this.port;
	}

}
