package systemes.repartie.tree.ftp.Arbre;

import java.io.IOException;
import java.util.ArrayList;

import systemes.repartie.tree.ftp.FtpConnection.Connection;
/**
 * class to display the tree of our repetory
 * @author hocine
 *
 */
public class Arbre {
	// variable declaration
	private Connection connect;
	//constructor
	public Arbre(Connection connect) {
		this.connect=connect;
	}
	//getter
	public Connection getConnection() {
		return this.connect;
	}
	
	
	/*
	 * brows our repertory and display the tree of every file, directory or link using TypeOf methode
	 * params space : to put space between parent and children files 
	 * @throws IOException
	 */
	public void getTree(String space) throws IOException {
		this.connect.envoyerType();
		String mySTring = this.connect.envoyerPASV();
		String adrs= this.connect.getAdressePasv(mySTring);
		int port = this.connect.getPORTPasv(mySTring);
		ArrayList<String> Readl=this.connect.envoyerList(adrs,port);
		this.connect.envoyerPWD();
		for(int i=0; i<Readl.size();i++) {
			if(Readl.get(i)!=null) {
			TypeOf(Readl.get(i),space);
			}
		}
	}
	
	/*
	 * Display the type of file in the param given (file "F",Directory "D" or Link "L"
	 * param space : to put space between parent and children files 
	 * param s : pemission and type of file 
	 * @throws IOException
	 */
	public void TypeOf(String s,String space) throws IOException {
		String[] myline=NameOperation(s);
		
		if (myline[0].startsWith("d")) {
			System.out.println("* "+space+"D--->>> " +myline[8]);
			boolean enter=this.connect.envoyerCWD(myline[8]);
			if (enter) {
				getTree(space+"   ");
				this.connect.envoyerCdup();
			}
			
		}
		else if (myline[0].startsWith("l")) {
			System.out.println("* "+space+"L------->> " +myline[myline.length-1]);
		}
		else {
			System.out.println("* "+space+"F-------> " +myline[8]);
		}
	}
	
	
	
	/*
	 * Operation on String param to delete space and put "," between parts and finaly make it String[] 
	 * params s 
	 * @return String[]
	 */
	public String[] NameOperation(String s){
		s=s.replaceAll(" ",",");
		while(s.indexOf(",,")!=-1) {
			s=s.replaceAll(",,",",");
		}
		return s.split(",");
	}

}
