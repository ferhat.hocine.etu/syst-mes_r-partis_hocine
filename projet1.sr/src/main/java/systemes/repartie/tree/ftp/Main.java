package systemes.repartie.tree.ftp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import systemes.repartie.tree.ftp.Arbre.Arbre;
import systemes.repartie.tree.ftp.FtpConnection.Connection;
import systemes.repartie.tree.ftp.ParametreConnection.ConnectionParametre;
/**
 * Main
 * @author hocine
 *
 */
public class Main {
	public static void main(String[] args) throws IOException {
		String user="anonymous";
		String pass="anonymous";
		if(args.length>1) {
			user=args[1];
		}
		if(args.length>2) {
			pass=args[2];
		}
		ConnectionParametre connectPar= new ConnectionParametre(user,pass,21,args[0]);
		Connection connect= new Connection(connectPar);
		System.out.println("-------------Connection-------------");
		connect.OperationConnection();
		System.out.println("\n");
		System.out.println("--------------* Tree *--------------");
		Arbre ar= new Arbre(connect);
		ar.getTree("");
		System.out.println("------------------------------------");

	}
}
