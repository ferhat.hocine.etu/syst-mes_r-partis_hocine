package systemes.repartie.tree.ftp.FtpConnection;
import org.junit.Test;

import systemes.repartie.tree.ftp.ParametreConnection.ConnectionParametre;

import static org.junit.Assert.*;

import java.io.IOException;

public class ConnectionTest {
	public static ConnectionParametre connect=new ConnectionParametre("anonymous","anonymous",21,"ftp.ubuntu.com");
	public static Connection connectFtp= new Connection(connect);
	
	
	
	@Test
	public void getConnectionParamTest() {
		assertEquals(connectFtp.getConnectionParam().getIdentifiant(), "anonymous");
	}
	
	@Test
	public void envoyerCWD()  throws IOException {
		connectFtp.OperationConnection();
		assertFalse(connectFtp.envoyerCWD("nonExistingRepertory"));
	}
	
	@Test
	public void envoyerPASV_envoyerType()  throws IOException {
		connectFtp.OperationConnection();
		connectFtp.envoyerType();
		connectFtp.OperationConnection();
		String line =connectFtp.envoyerPASV();
		assertTrue(line.startsWith("227 Entering Passive Mode"));

	}
	
}
