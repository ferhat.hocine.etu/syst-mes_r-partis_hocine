package systemes.repartie.tree.ftp.ParametreConnection;
import org.junit.Test;
import static org.junit.Assert.*;

public class ConnectionParametreTest {
	public static ConnectionParametre connect=new ConnectionParametre("anonymous","anonymous",21,"ftp.ubuntu.com");
	
	
	@Test
	public void getIdentifiantTest() {
		assertEquals(connect.getIdentifiant(), "anonymous");
	}
	
	@Test
	public void getMotdepasseTest() {
		assertEquals(connect.getMotdepasse(), "anonymous");
	}
	
	@Test
	public void getAdresseTest() {
		assertEquals(connect.getAdresse(), "ftp.ubuntu.com");
	}
	
	@Test
	public void getPortTest() {
		assertEquals(connect.getPort(), 21);
	}
}
