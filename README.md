Projet nº1 - Application «Tree FTP»
Ferhat HOCINE GL02
2021

*** 1/ Introduction
I)objectif:
L'Application «Tree FTP» a pour résultat l'affichage de l'arborescence d'un répertoire accessible via le protocole applicatif (FTP) depuis un shell graçe a une commande.
cette commande prend en paramètre obligatoirement l'adresse du serveur FTP + en option un nom d'utilisateur et un mot de passe

II)exécution:
Dans le fichier racine on exécute la commande :
->>> mvn package      
//pour compliler le projet
puis la commande :
->>> mvn javadoc:javadoc
//pour générer la documentation:
puis la commande :
java -jar target/projet1.sr-1.0-SNAPSHOT.jar ftp.ubuntu.com
//pour lancer l'application sur le server FTP ubuntu 
ou la commande :
java -jar target/projet1.sr-1.0-SNAPSHOT.jar ftp.free.fr
//pour le server FTP Free


*** 2/ Architecture
la gestion d'erreur:
La plus part des méthodes de la classe connection renvoie IOException avec un message en cas d'erreur.
exemple:
la méthode envoyerPWD() qui envoie la commande "PWD"

	public void envoyerPWD()  throws IOException {
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
		}
		else {
		printer.write("PWD\r\n");
		printer.flush();
		try {
		String r = this.bfR.readLine();
		}
		catch(IOException e) {
			throw new IOException("Buffer reader error");
		}
		}
	}
	
Dans la méthode envoyerPWD lors de la lecture de la réponse par le buffer reader en cas de IOException il sera attraper par le catch et renvera un new IOException avec le message "Buffer reader error".
la capture d'erreure est de même pour toutes les méthode de la classe connection


*** 3/ Parcours du code (code samples)


a) la méthode envoyerCWD(String dossier) de la classe Connection::

cette méthode utilise le String dossier passé en paramètre pour envoyé un cammande  CWD +dossier afin de savoir si on peut accédé ou dossier dant le nom est passé en paramètre ou l'accès est interdit et la réponse est renvoyé sous forme de boolean par la méthode.
	
	public boolean envoyerCWD(String dossier) throws IOException  {
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
		}
		else {
			try {
				printer.write("CWD "+dossier+"\r\n");
				printer.flush();
				String r = this.bfR.readLine();
				return r.startsWith("250");
				
				}
				catch(IOException e) {
						throw new IOException("Buffer reader error");
					}
				catch (NullPointerException e){
					return false;
			}
		}
		return false;
	}
	
	
b) la méthode envoyerPASV() de la classe Connection:
cette méthode envoie la commande PASV afin de passer au mode passive. en cas de réussite les information récupérer sont renvoyé dans le cas contraire l'exception est levée.

 	public String envoyerPASV() throws IOException  {
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
			return "";
		}
		else {
		try {
			printer.write("PASV\r\n");
			printer.flush();
			String r = this.bfR.readLine();
			return r;
		}
		catch(IOException e) {
			throw new IOException("Buffer reader error");
		}
		}
	}
	
	
c) la méthode getAdressePasv() de la classe Connection:
cette méthode utilise le string (PASV) passé en paramètre qui représente une réponse de la commande PASV et extrait l'adresse IP compris dans le message.


	public String getAdressePasv(String PASV) {
		int i = PASV.indexOf("(");
		int j = PASV.indexOf(")");
		PASV= PASV.substring(i+1,j);
		String [] str=PASV.split(",");
		String res="";
		for (i=0; i<4; i++) {
			res+=str[i]+".";
		}
		res=res.substring(0,res.length()-1);
		return res;
		}	

d) la méthode envoyerList(String adrs, int port) de la classe Connection:
cette methode utilise l'adresse ip et le port (passées en paramètre )extrait depuis la réponse de la commande PASV afin de retourner la liste des élements contenu dans la dossier encours

	public ArrayList<String> envoyerList(String adrs, int port) throws IOException  {
		ArrayList<String> Readl=new ArrayList<String>();
		if (this.socket==null) {
			System.out.println("Erreur connection FTP Server !!");
			return Readl;
		}
		else {
			try {
				printer.write("LIST\r\n");
				printer.flush();
				Readl=this.newConnection(adrs, port);
				String r = this.bfR.readLine();
				r = this.bfR.readLine();
				
				return Readl;
			}
		
			catch(IOException e) {
				throw new IOException("Buffer reader error");
			}
		}
	}


e)la méthode NameOperation(String s)de la classe Arbre:
cette méthode transforme le string passé en paramètre qui représentre un ligne d'information d'un élement(fichier, dossier, lien ...) en liste avec les partie séparées et dans espace.

	public String[] NameOperation(String s){
		s=s.replaceAll(" ",",");
		while(s.indexOf(",,")!=-1) {
			s=s.replaceAll(",,",",");
		}
		return s.split(",");
	}	
